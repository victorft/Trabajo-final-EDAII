#include "dispersion.h"
#include <string.h>

int main()
{
	char *f1 = "alumnosC.hash";
	char *f2 = "alumnos.dat";
	char dni[10];
	tipoAlumno *alumno;
	int cubo, posicion, cuboDes, err, rt;
	FILE *f;

	regConfig reg = {10, 4, 10, 75, 40, 0, 0};

	creaHvacio(f1, &reg);

	if ((rt = creaHash(f2, f1, &reg)))
	{
		if (rt == -3 || rt == -4)
			fprintf(stderr, "---------- No se respeta la densidad indicada ------------\n\n");
		else
		{

			fprintf(stderr, "Error creado el fichero hash");
			return 1;
		}
	}

	leeHash(f1);

	if ((f = fopen(f1, "rb")) == NULL)
	{
		fprintf(stderr, "Error abriendo el fichero");
		return 1;
	}

	strcpy(dni, "11929273");

	if ((alumno = busquedaHash(f, dni, &cubo, &cuboDes, &posicion, &err)) == NULL)
		fprintf(stderr, "\nNo se encontro el alumno con DNI %s - ERRCODE %d\n", dni, err);
	else
	{
		printf("\n-------- REGISTRO ENCONTRADO -----------\n");
		printf("%-10s%-20s%-40s%-12s\n", "DNI", "NOMBRE", "APELLIDOS", "PROVINCIA");
		printf("%-10s%-20s%-20s%-20s%-12s\n",
			   alumno->dni,
			   alumno->nombre,
			   alumno->ape1,
			   alumno->ape2,
			   alumno->provincia);
		printf("Se ha encontrado en el cubo %d. Desborde %d y en la fila %d\n", cubo, cuboDes, posicion);
	}

	strcpy(dni, "33333333");

	if ((alumno = busquedaHash(f, dni, &cubo, &cuboDes, &posicion, &err)) == NULL)
		fprintf(stderr, "\nNo se encontro el alumno con DNI %s - ERRCODE %d\n", dni, err);
	else
	{
		printf("\n-------- REGISTRO ENCONTRADO -----------\n");
		printf("%-10s%-20s%-40s%-12s\n", "DNI", "NOMBRE", "APELLIDOS", "PROVINCIA");
		printf("%-10s%-20s%-20s%-20s%-12s\n",
			   alumno->dni,
			   alumno->nombre,
			   alumno->ape1,
			   alumno->ape2,
			   alumno->provincia);
		printf("Se ha encontrado en el cubo %d. Desborde %d y en la fila %d\n", cubo, cuboDes, posicion);
	}

	strcpy(dni, "4568418");

	if ((alumno = busquedaHash(f, dni, &cubo, &cuboDes, &posicion, &err)) == NULL)
		fprintf(stderr, "\nNo se encontro el alumno con DNI %s - ERRCODE %d\n", dni, err);
	else
	{
		printf("\n-------- REGISTRO ENCONTRADO -----------\n");
		printf("%-10s%-20s%-40s%-12s\n", "DNI", "NOMBRE", "APELLIDOS", "PROVINCIA");
		printf("%-10s%-20s%-20s%-20s%-12s\n",
			   alumno->dni,
			   alumno->nombre,
			   alumno->ape1,
			   alumno->ape2,
			   alumno->provincia);
		printf("Se ha encontrado en el cubo %d. Desborde %d y en la fila %d\n", cubo, cuboDes, posicion);
	}

	strcpy(dni, "7453920");

	if ((alumno = busquedaHash(f, dni, &cubo, &cuboDes, &posicion, &err)) == NULL)
		fprintf(stderr, "\nNo se encontro el alumno con DNI %s - ERRCODE %d\n", dni, err);
	else
	{
		printf("\n-------- REGISTRO ENCONTRADO -----------\n");
		printf("%-10s%-20s%-40s%-12s\n", "DNI", "NOMBRE", "APELLIDOS", "PROVINCIA");
		printf("%-10s%-20s%-20s%-20s%-12s\n",
			   alumno->dni,
			   alumno->nombre,
			   alumno->ape1,
			   alumno->ape2,
			   alumno->provincia);
		printf("Se ha encontrado en el cubo %d. Desborde %d y en la fila %d\n", cubo, cuboDes, posicion);
	}

	fclose(f);

	if ((cubo = modificarReg(f1, "7453920", "MI CASA")) < 0)
		fprintf(stderr, "\nNo se pudo modificar el registro con DNI 7453920\n");
	else
		printf("\nSe modifico el campo con exito (7453920 --> %d)\n", cubo);

	if ((cubo = modificarReg(f1, "33333333", "TU CASA")) < 0)
		fprintf(stderr, "\nNo se pudo modificar el registro con DNI 33333333\n");
	else
		printf("\nSe modifico el campo con exito (33333333 --> %d)\n", cubo);

	printf("\n\n");

	leeHash(f1);

	return 0;
}
