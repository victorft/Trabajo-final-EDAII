CC=gcc

all: pruebaDispersion limpiar

pruebaDispersion: pruebaDispersion.c dispersion.o
	$(CC) -g $^ -o $@.out

dispersion.o: dispersion.c dispersion.h
	$(CC) -c -g $< -o $@

limpiar:
	rm *.o
