#include "dispersion.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Lee el contenido del fichero hash organizado mediante el método de DISPERSIÓN según los criterios
// especificados en la práctica. Se leen todos los cubos completos tengan registros asignados o no. La
// salida que produce esta función permite visualizar el método de DISPERSIÓN

int leeHash(char *fichHash)
{
	FILE *f;
	tipoCubo cubo;
	regConfig regC;
	int j, nCubo = 0, densidadOcupacion;

	f = fopen(fichHash, "rb");
	fread(&regC, sizeof(regConfig), 1, f);
	fread(&cubo, sizeof(cubo), 1, f);

	while (!feof(f))
	{
		for (j = 0; j < C; j++)
		{
			if (j == 0)
				printf("Cubo %2d (%2d reg. ASIGNADOS)", nCubo, cubo.numRegAsignados);
			else if ((j == 1) && cubo.desbordado)
				printf("DESBORDADO\t\t");
			else
				printf("\t\t\t");
			if (j < cubo.numRegAsignados)
				printf("\t%s %s %s %s %s\n", cubo.reg[j].dni, cubo.reg[j].nombre, cubo.reg[j].ape1, cubo.reg[j].ape2,
					   cubo.reg[j].provincia);
			else
				printf("\n");
		}
		nCubo++;

		fread(&cubo, sizeof(cubo), 1, f);
	}

	fclose(f);
	printf("ORGANIZACIÓN MÉTODO DISPERSIÓN:\n\t %d CUBOS y %d CUBOS DESBORDE con capacidad %d\n",
		   regC.nCubos, regC.nCubosDes, C);

	printf("\t Contiene %d registros de los cuales se han desbordado %d:\n", regC.numReg, regC.numRegDes);
	densidadOcupacion = 100 * regC.numReg / ((regC.nCubos + regC.nCubosDes) * C);

	printf("Densidad ACTUAL de ocupación: %d \n\t(MÍNIMA permitida %d  MÁXIMA permitida %d)\n",
		   densidadOcupacion, regC.densidadMin, regC.densidadMax);

	if (densidadOcupacion > regC.densidadMax)
	{
		printf("No se respeta la densidad máxima de ocupacion\n");
		return -3;
	}

	if (densidadOcupacion < regC.densidadMin)
	{
		printf("No se respeta la densidad mínima de ocupacion\n");
		return -4;
	}

	if ((densidadOcupacion > regC.densidadMin) && (densidadOcupacion < regC.densidadMax))
		return 0;

	return -1;
}

int creaHash(char *fichEntrada, char *fichHash, regConfig *regC)
{
	FILE *input, *output;
	tipoAlumno alumno;
	tipoCubo cubo;
	int hash;
	int densidadOcupacion;

	if ((input = fopen(fichEntrada, "rb")) == NULL)
		return -1;

	if ((output = fopen(fichHash, "r+b")) == NULL)
	{
		fclose(input);
		return -2;
	}

	// Se recorren secuancialmente todos los alumnos
	while (fread(&alumno, sizeof(tipoAlumno), 1, input) != 0)
	{
		if ((hash = getHash(atoi(alumno.dni), regC->nCubos)) == -1)
		{
			CLOSE_FILES(input, output, -5)
		}

		if (fseek(output, sizeof(regConfig) + sizeof(tipoCubo) * hash, SEEK_SET) == -1)
		{
			CLOSE_FILES(input, output, -2)
		}

		if (fread(&cubo, sizeof(tipoCubo), 1, output) < 1)
		{
			CLOSE_FILES(input, output, -2)
		}

		++(regC->numReg);

		if (cubo.numRegAsignados < C)
		{
			cubo.reg[cubo.numRegAsignados++] = alumno;

			if (fseek(output, sizeof(tipoCubo) * -1, SEEK_CUR) == -1)
			{
				CLOSE_FILES(input, output, -2)
			}

			if (fwrite(&cubo, sizeof(tipoCubo), 1, output) < 1)
			{
				CLOSE_FILES(input, output, -2)
			}
		}
		else
		{
			cubo.desbordado = 1;
			if (fseek(output, sizeof(tipoCubo) * -1, SEEK_CUR) == -1)
			{
				CLOSE_FILES(input, output, -2)
			}

			if (fwrite(&cubo, sizeof(tipoCubo), 1, output) < 1)
			{
				CLOSE_FILES(input, output, -2)
			}

			++(regC->numRegDes);
			desborde(output, &alumno, regC);
		}

		rewind(output);

		if (fwrite(regC, sizeof(regConfig), 1, output) < 1)
		{
			CLOSE_FILES(input, output, -2)
		}
	}

	densidadOcupacion = 100 * regC->numReg / ((regC->nCubos + regC->nCubosDes) * C);

	if (densidadOcupacion > regC->densidadMax)
	{
		CLOSE_FILES(input, output, -3)
	}

	if (densidadOcupacion < regC->densidadMin)
	{
		CLOSE_FILES(input, output, -4)
	}

	CLOSE_FILES(input, output, 0)
}

void creaHvacio(char *fichHash, regConfig *reg)
{
	FILE *salida;
	tipoCubo cubo;
	int i;

	if (fichHash == NULL || reg == NULL)
	{
		fprintf(stderr, "CREAVACIO: Los parametros introducidos no son correctos, saliendo...\n");
		return;
	}

	if ((salida = fopen(fichHash, "wb")) == NULL)
	{
		fprintf(stderr, "CREAVACIO: Error creando el fichero\n");
		return;
	}

	if (fwrite(reg, sizeof(regConfig), 1, salida) < 1)
	{
		fprintf(stderr, "CREAVACIO: Error escribiendo la cabecera en el fichero vacio.\n");
		fclose(salida);
		return;
	}

	memset(&cubo, 0, sizeof(tipoCubo));

	for (i = 0; i < (reg->nCubos + reg->nCubosDes); i++)
		if (fwrite(&cubo, sizeof(tipoCubo), 1, salida) < 1)
		{
			fprintf(stderr, "CREAVACIO: Error creado el fichero vacio\n");
			fclose(salida);
			return;
		}

	fclose(salida);
}

void desborde(FILE *fHash, tipoAlumno *reg, regConfig *regC)
{
	tipoCubo cubo;

	if (fseek(fHash, sizeof(regConfig) + sizeof(tipoCubo) * regC->nCuboDesAct, SEEK_SET) == -1)
	{
		fprintf(stderr, "DESBORDE: Error realizando el desplazamiento a la zona de desbordamiento\n");
		return;
	}

	if (fread(&cubo, sizeof(tipoCubo), 1, fHash) < 1)
	{
		fprintf(stderr, "DESBORDE: Error realizando la lectura del fichero hash\n");
		return;
	}

	if (fseek(fHash, sizeof(tipoCubo) * -1, SEEK_CUR) == -1)
	{
		fprintf(stderr, "DESBORDE: Error moviendo el cursor a la posicion anterior\n");
		return;
	}

	cubo.reg[cubo.numRegAsignados++] = *reg;

	if (fwrite(&cubo, sizeof(tipoCubo), 1, fHash) < 1)
	{
		fprintf(stderr, "DESBORDE: Error escribiendo los datos en el fichero\n");
		return;
	}

	if (cubo.numRegAsignados == C && regC->nCuboDesAct == (regC->nCubos + regC->nCubosDes - 1))
	{
		regC->nCubosDes++;
		regC->nCuboDesAct++;
		memset(&cubo, 0, sizeof(tipoCubo));

		if (fwrite(&cubo, sizeof(tipoCubo), 1, fHash) < 1)
		{
			fprintf(stderr, "DESBORDE: Error ampliando el desborde\n");
			return;
		}

		return;
	}

	if (cubo.numRegAsignados == C)
		regC->nCuboDesAct++;
}

tipoAlumno *busquedaHash(FILE *f, char *dni, int *nCubo, int *nCuboDes, int *posReg, int *error)
{
	int i;
	tipoAlumno *alumno = NULL;
	tipoCubo cubo;
	regConfig reg;

	if (f == NULL)
	{
		*error = -2;
		return NULL;
	}

	if (dni == NULL || nCubo == NULL || nCuboDes == NULL || posReg == NULL)
	{
		*error = -4;
		return NULL;
	}

	rewind(f);

	if (fread(&reg, sizeof(regConfig), 1, f) < 1)
	{
		*error = -2;
		return NULL;
	}

	if ((*nCubo = getHash(atoi(dni), reg.nCubos)) == -1)
	{
		*error = -4;
		return NULL;
	}

	if (fseek(f, sizeof(tipoCubo) * *nCubo, SEEK_CUR) == -1)
	{
		*error = -2;
		return NULL;
	}

	if (fread(&cubo, sizeof(tipoCubo), 1, f) < 1)
	{
		*error = -2;
		return NULL;
	}

	for (i = 0; i < cubo.numRegAsignados; i++)
	{
		if (!strcmp(cubo.reg[i].dni, dni))
		{
			if ((alumno = malloc(sizeof(tipoAlumno))) == NULL)
			{
				*error = -4;
				return NULL;
			}

			*nCuboDes = -1;
			*posReg = i;

			memcpy(alumno, &cubo.reg[i], sizeof(tipoAlumno));

			*error = 0;

			return alumno;
		}
	}

	if (fseek(f, sizeof(regConfig) + sizeof(tipoCubo) * reg.nCubos, SEEK_SET) == -1)
	{
		*error = -1;
		return NULL;
	}

	*nCuboDes = 0;

	while (fread(&cubo, sizeof(tipoCubo), 1, f) > 0)
	{
		for (i = 0; i < cubo.numRegAsignados; i++)
			if (!strcmp(cubo.reg[i].dni, dni))
			{
				if ((alumno = malloc(sizeof(tipoAlumno))) == NULL)
				{
					*error = -4;
					return NULL;
				}

				*posReg = i;

				memcpy(alumno, &cubo.reg[i], sizeof(tipoAlumno));

				*error = 0;

				return alumno;
			}

		(*nCuboDes)++;
	}

	*error = -1;
	return NULL;
}

int modificarReg(char *fichero, char *dni, char *provincia)
{
	int nCubo, nCuboDes, posReg, err;
	tipoAlumno *alumno;
	regConfig reg;
	FILE *f;

	if (provincia == NULL)
		return -4;

	if ((f = fopen(fichero, "r+b")) == NULL)
		return -2;

	if (fread(&reg, sizeof(regConfig), 1, f) < 1)
	{
		fclose(f);
		return -2;
	}

	alumno = busquedaHash(f, dni, &nCubo, &nCuboDes, &posReg, &err);

	if (err)
	{
		fclose(f);
		return err;
	}

	strcpy(alumno->provincia, provincia);

	if (nCuboDes == -1)
	{
		if (fseek(f, sizeof(regConfig) + sizeof(tipoCubo) * nCubo + sizeof(tipoAlumno) * posReg, SEEK_SET) == -1)
		{
			free(alumno);
			fclose(f);
			return -2;
		}

		if (fwrite(alumno, sizeof(tipoAlumno), 1, f) < 1)
		{
			free(alumno);
			fclose(f);
			return -2;
		}

		fclose(f);
		return nCubo;
	}

	if (fseek(f, sizeof(regConfig) + sizeof(tipoCubo) * reg.nCubos + sizeof(tipoCubo) * nCuboDes + sizeof(tipoAlumno) * posReg, SEEK_SET) == -1)
	{
		free(alumno);
		fclose(f);
		return -2;
	}

	if (fwrite(alumno, sizeof(tipoAlumno), 1, f) < 1)
	{
		free(alumno);
		fclose(f);
		return -2;
	}

	free(alumno);
	fclose(f);
	return reg.nCubos + nCuboDes;
}

int getHash(int clave, int nCubos)
{
	if (clave < 0 || nCubos <= 0)
		return -1;

	return clave % nCubos;
}
